import java.io.*;
import java.lang.StringBuilder;

class hashFunctions
{
	//the 64 binary words Ki given by the 32 first bits of the fractional parts of the cube roots of the first 64 prime numbers:
	int[] binaryKi = {0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174, 0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da, 0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85, 0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070, 0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2};
	
	//Addition Modulo 2^32 function boundary
	long Bound = 0x100000000L;
	
	//number of blocks of 512 bit words of input message
	int numberOfBlocks = 0;
	
	//Elementary hashFunctions
	
	long Ch_function(long X, long Y, long Z)
	{
		return (X&Y)^(~X&Z);
	}
	long Maj_function(long X, long Y, long Z)
	{
		return (X&Y)^(X&Z)^(Y&Z);
	}
	long IntegerAdditionModulo2Pow32(long A, long B)
	{
		return (A+B)%Bound;
	}
	long RotR(long bits, long n)
	{
		return (bits >>> n) | (bits << (Integer.SIZE-n));
	}
	long ShR(long bits, long n)
	{
		return bits>>>n;
	}
	//This function is not used and can be removed.
	String A_concate_B(long A, long B, int base)
	{
		StringBuilder str = new StringBuilder();
		str.append(Long.toString(A, base));
		str.append(Long.toString(B, base));
		return str.toString();
	}
	long GreaterSigma_Zero_function(long X)
	{
		return RotR(X,2)^RotR(X,13)^RotR(X,22);
	}
	long GreaterSigma_One_function(long X)
	{
		return RotR(X,6)^RotR(X,11)^RotR(X,25);
	}
	long LowerSigma_Zero_function(long X)
	{
		return RotR(X,7)^RotR(X,18)^RotR(X,3);
	}
	long LowerSigma_One_function(long X)
	{
		return RotR(X,17)^RotR(X,19)^RotR(X,10);
	}
	
	//Aggregate hashFunctions:
	
	String[] fetchSlicedData(String fileText)
	{
		//This function does the pre-Block-Decomposition.
		/*
		Since one HexaDecimal number is equal to 4 binary digits; therefore, 512 binary digits isEquivalentTo 128 HexaDecimal digits.
		Hence, I divide our input file into blocks of 128 hexaDigits (equivalent to blocks of 512 bits).
		*/
		String Text = fileText;
		
		Boolean notFillPadding = Text.length()%128 == 0;	//128 HexaDecimals isEquivalentTo 512 bits
		if(!notFillPadding)
		{
			numberOfBlocks=(int)Math.floor(Text.length()/128)+1;
			//Fill Padding Here!  I'll fill the last element with padding here before breaking into blocks.
			Text += hashPaddingFunction(Text.length());
			//System.out.println(Text);
		}
		else
		{
			numberOfBlocks=(int)Math.floor(Text.length()/128);
		}
		
		//I divide the whole string of input into cluster of 128-HexaDecimal digits.
		String[] ClusterOfInputBlocks = new String[numberOfBlocks];
		
		//Now I fill the 'ClusterOfInputBlocks' array with all the elements.
		StringBuilder aggregateString = new StringBuilder();
		for(int i=0;i< numberOfBlocks;i++)
		{
			aggregateString = new StringBuilder();
			for(int j=0;j<128;j++)
			{
				aggregateString.append(Text.charAt(j+i*128));
				ClusterOfInputBlocks[i] = aggregateString.toString();
			}
		}
		return ClusterOfInputBlocks;
	}
	
	String hashPaddingFunction(int textLength)	//I'll use the 'numberOfBlocks' variable to decide for padding.
	{
		//Simple padding function to test run the algorithm.
		StringBuilder padStr = new StringBuilder();
		
		/*
		for(int i = 0;i<(numberOfBlocks*128) - textLength; i++)
		{
			padStr.append("0");
		}
		*/
		//Test case Running successfully
		
	//Calculating the Step#1, Step#2 and Step#3 Padding in binary, will convert later to HexaDecimal padding.
		//Step#1 - pad first bit as '1'
		padStr.append("1");
		
		//Step#2 - pad k bits as '0'; such that textLength+1+k isEquivalentTo 448mod512.
		//Searching 'k'.	//'k' is the number of binary digit paddings.
		int originalTextLengthInBits = textLength*4;
		int textLengthInBits = originalTextLengthInBits + 1;
		//System.out.println("textLength in number of binary digits after appending '1' = " + textLengthInBits);
		int k=0;
		while((k+textLengthInBits)%512!=448)
		{
			k+=1;
		}
		//System.out.println("k required: " + k);
		for(int i = 0;i<k;i++)
		{
			padStr.append("0");
		}
		//int appendedTextLength = textLengthInBits+k;
		//System.out.println("Text length after appending '1' and k '0' bits: " + appendedTextLength);
		
		//Step#3 - Now only 64 last bits for padding are remaining. Therefore, I append 'originalTextLengthInBits' in binary in 64 bit long representation to the 'padStr'.
		//I use int variable 'p' to represent this pad in program.
		String p = Integer.toString(originalTextLengthInBits,2);
		//System.out.println("Value of l<2^64 padding: " + p);
		
		int numberOfZeros = 64-p.length();	//number of zeros required before 'p'.
		//System.out.println("number of zeros required before 'p': " + numberOfZeros);
		for(int i = 0;i<numberOfZeros;i++)
		{
			padStr.append("0");
		}
		padStr.append(p);
		
		//Conversion from binary to hexadecimal before appending.
		//System.out.println("text length after padding: " + (originalTextLengthInBits + padStr.length()));
		//System.out.println(padStr.toString() + "\nLength of Pad-String in bits: " + padStr.length());
		String[] hexPadArray = new String[padStr.length()/4];
		StringBuilder tempStore;
		for(int i=0;i< hexPadArray.length;i++)
		{
			tempStore = new StringBuilder();
			tempStore.append(padStr.toString().charAt(0+i*4));
			tempStore.append(padStr.toString().charAt(1+i*4));
			tempStore.append(padStr.toString().charAt(2+i*4));
			tempStore.append(padStr.toString().charAt(3+i*4));
			hexPadArray[i]=Integer.toString(Integer.parseInt(tempStore.toString(),2),16);
			//System.out.println(hexPadArray[i]);
		}
		
		padStr = new StringBuilder();
		for(int i = 0;i<hexPadArray.length;i++)
		{
			padStr.append(hexPadArray[i]);
		}
		return padStr.toString();
	}
}

class writeFileClass
{
	boolean writeFile(String fileName, String writeData)
	{
        try {
            // Used wrap FileWriter in BufferedWriter.
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName));

            bufferedWriter.write(writeData);
            
			//Closing file.
            bufferedWriter.close();
			return true;
        }
        catch(IOException ex) {
            System.out.println("Error writing to file '" + fileName + "'");
            ex.printStackTrace();
			return false;
        }
	}
}

class readFileClass
{
	String readFile(String fileName)
	{
        // This will reference one line at a time
        String line = null;
		StringBuilder fullText = new StringBuilder();

        try 
		{
            // Used wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));

            while((line = bufferedReader.readLine()) != null)
			{
                fullText.append(line);
            }   

            //Closing files.
            bufferedReader.close();
			return fullText.toString().replaceAll("\\s+","");
        }
        catch(FileNotFoundException ex)
		{
            System.out.println("Unable to open file '" + fileName + "'");
			return null;
        }
        catch(IOException ex)
		{
            System.out.println("Error reading file '" + fileName + "'");
            ex.printStackTrace();
			return null;
        }
    }
}

class Block64Words
{
	int[] Words;	//integer is of 32-bits, therefore, words are of 32-bits each.
	Block64Words(String M512bits)
	{
		//Constructing First 16 Words
		Words = new int[64];
		//System.out.println(M512bits);
		StringBuilder tempIntStore;
		for(int i =0;i<16;i++)
		{
			tempIntStore=new StringBuilder();
			for(int j=0;j<8;j++)
			{
				tempIntStore.append(M512bits.charAt(i*8+j));
			}
			//System.out.println("#" + i + ": tempIntStore : " + tempIntStore.toString());
			//Words[i]=Integer.parseInt(tempIntStore.toString(),16);	//Gives me fatal error - because "Integer.toHexString returns a string representation of the integer as an unsigned value - while Integer.parseInt takes a signed int."
			Words[i] = (int)Long.parseLong(tempIntStore.toString(), 16);
			//System.out.println("#" + i + Words[i]);
		}
		
		//Constructing 17-63 Words
		hashFunctions func = new hashFunctions();
		for(int i=17;i<64;i++)
		{
			Words[i] = (int)(func.IntegerAdditionModulo2Pow32(func.IntegerAdditionModulo2Pow32(func.IntegerAdditionModulo2Pow32(func.LowerSigma_One_function(Words[i-2]),Words[i-7]),func.LowerSigma_Zero_function(Words[i-15])),Words[i-16]));
			//System.out.println("#" + i + ": " + Words[i]);
		}
	}
}

class SHA256
{
	static final int numberOfRounds = 64;
	int[] abcd = new int[8];
	int T1=0; int T2=0;
	hashFunctions func;
	int[] newIV = new int[8];
	int[] Hash(Block64Words B, int[] iv)
	{	
		/*Simple test case
		for(int i = 0;i<B.Words.length;i++)
		{
			System.out.println(B.Words[i]);
		}
		return B.Words;
		*/
		//Test case Successful!
		 func = new hashFunctions();
		for(byte i = 0;i<8;i++)
		{
			newIV[i]=abcd[i]=iv[i];
		}
		for(int i = 0; i<numberOfRounds; i++)
		{
			if(i!=0)
			{
				T1 = ((int)func.IntegerAdditionModulo2Pow32(func.IntegerAdditionModulo2Pow32(func.IntegerAdditionModulo2Pow32(func.IntegerAdditionModulo2Pow32(abcd[7],func.GreaterSigma_One_function(abcd[4])),func.binaryKi[i]),func.Ch_function(abcd[4],abcd[5],abcd[6])),B.Words[i]));
				T2 = ((int)func.IntegerAdditionModulo2Pow32(func.GreaterSigma_Zero_function(abcd[0]),func.Maj_function(abcd[0],abcd[1],abcd[2])));
				abcd[7]=abcd[6];	//7 -- h
				abcd[6]=abcd[5];	//6 -- g
				abcd[5]=abcd[4];	//5 -- f
				abcd[4]=((int)func.IntegerAdditionModulo2Pow32(abcd[3],T1));	//4 -- e
				abcd[3]=abcd[2];	//3 -- d
				abcd[2]=abcd[1];	//2 -- c
				abcd[1]=abcd[0];	//1 -- b
				abcd[0]=((int)func.IntegerAdditionModulo2Pow32(T1,T2));	//0 -- a
			}
		}
		
		//Again computing new iv to return
		for(int i = 0;i<newIV.length;i++)
		{
			newIV[i] = ((int)func.IntegerAdditionModulo2Pow32(iv[i],abcd[3]));
		}
		return newIV;
	}
}

class HMAC	//psvm class
{
	StringBuilder hashDigest = new StringBuilder();
	int iPad = 0x36363636;
	int oPad = 0x5C5C5C5C;
	int intKey;
	
	String paddedXORedKey(String key, char c)
	{
		StringBuilder tempKey = new StringBuilder();
		//Pad to fit bitsize of iPad/oPad.
		if(key.length()<8)
		{
			tempKey.append(key);
			for(byte i = 0;i<(8-key.length());i++)
			{
				tempKey.append("0");
			}
			key = tempKey.toString();
		}
		intKey = (int)Long.parseLong(tempKey.toString(), 16);
		
		//XORing function with iPad/oPad
		if(c == 'i')
		{
			intKey=intKey^iPad;
		}
		else
		{
			intKey=intKey^oPad;
		}
		
		//Convert to String and Pad again to fit 512 bits.	//This component can be removed.
		key = Integer.toHexString(intKey);
		tempKey = new StringBuilder();
		if(key.length()<128)
		{
			tempKey.append(key);
			for(byte i = 0;i<(128-key.length());i++)
			{
				tempKey.append("0");
			}
			key = tempKey.toString();
		}
		return key;
	}
	public static void main(String args[])
	{
		//Common Variables:
		String[] strArray;
		Block64Words[] MBlocks;
		SHA256 sha;
		
		//CALCULATE SHA-256:
		
		int[] firstIV = {0x6a09e667,0xbb67ae85,0x3c6ef372,0xa54ff53a,0x510e527f,0x9b05688c,0x1f83d9ab,0x5be0cd19};
		int[] oldIV = new int[8];
		for(int i = 0;i<8;i++)
		{
			oldIV[i]=firstIV[i];
		}
		/*
		////System.out.println(Long.parseLong(String.format("%x",f.binaryKi[2]),16));
		////System.out.println(f.Bound);
		////String testWrite = "Hello World. \nWrite Successful! " + Long.parseLong(String.format("%x",f.binaryKi[2]),16) + f.Bound;
		if((new writeFileClass()).writeFile("temp.java",testWrite))
		{
			System.out.println((new readFileClass()).readFile("temp.java"));
		}
		
		////System.out.println(f.fetchSlicedData("temp.java"));
		*/
		HMAC hmac = new HMAC();
		
		//HMAC-Step#1:
		String hashInput_iPad = hmac.paddedXORedKey((new readFileClass()).readFile(args[0]),'i')+(new readFileClass()).readFile(args[1]);
		
		strArray = (new hashFunctions()).fetchSlicedData(hashInput_iPad);	//Add the 'hmac.paddedXORedKey()' function here
		MBlocks = new Block64Words[strArray.length];
		sha = new SHA256();
		for(int x = 0; x<strArray.length;x++)
		{
			MBlocks[x] = new Block64Words(strArray[x]);
			/*
			//Call the Hash Function Here.
			//Assign new IV here
			//assigning : (a, b, c, d, e, f, g, h) = (H(t−1)1 ,H(t−1)2 ,H(t−1)3 ,H(t−1)4 ,H(t−1)5 , H(t−1)6 ,H(t−1)7 ,H(t−1)8 )
			//H(t-1)j is the intrinsic 'iv' value captured from last instance of the function. Therefore, I name 'abcd[]' as variable name.
			*/
			oldIV = sha.Hash(MBlocks[x],oldIV);
		}
		//SHA-256 CALCULATED
		//HashDigest to nextHash
		for(int i = 0;i<oldIV.length;i++)
		{
			hmac.hashDigest.append(Integer.toHexString(oldIV[i]));	//Don't forget to putit in Integer.toString
		}
		
		//HMAC-Step#2:
		String hashInput_oPad = hmac.paddedXORedKey((new readFileClass()).readFile(args[0]),'o')+hmac.hashDigest.toString();
		
		strArray = (new hashFunctions()).fetchSlicedData(hashInput_oPad);	//Add the 'hmac.paddedXORedKey()' function here
		MBlocks = new Block64Words[strArray.length];
		sha = new SHA256();
		for(int x = 0; x<strArray.length;x++)
		{
			MBlocks[x] = new Block64Words(strArray[x]);
			/*
			//Call the Hash Function Here.
			//Assign new IV here
			//assigning : (a, b, c, d, e, f, g, h) = (H(t−1)1 ,H(t−1)2 ,H(t−1)3 ,H(t−1)4 ,H(t−1)5 , H(t−1)6 ,H(t−1)7 ,H(t−1)8 )
			//H(t-1)j is the intrinsic 'iv' value captured from last instance of the function. Therefore, I name 'abcd[]' as variable name.
			*/
			oldIV = sha.Hash(MBlocks[x],oldIV);
		}
		//SHA-256 CALCULATED
		//HashDigest to File and Displaying HashDigest
		hmac.hashDigest = new StringBuilder();
		for(int i = 0;i<oldIV.length;i++)
		{
			hmac.hashDigest.append(Integer.toHexString(oldIV[i])+" ");	//Don't forget to putit in Integer.toString
		}
		if((new writeFileClass()).writeFile(args[2],hmac.hashDigest.toString()))
		{
			System.out.println(hmac.hashDigest.toString());
		}
		////////////////////////////////////////////////////////////////////////
		//HMAC CALCULATED
	}
}
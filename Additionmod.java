class Additionmod
{
	long Bound = 0x100000000L;
	long IntegerAdditionModulo2Pow32(long A, long B)
	{
		return (A+B)%Bound;
	}
	public static void main(String[] args)
	{
		Additionmod am=new Additionmod();
		System.out.println(am.Bound);
		System.out.println(Long.toString(am.IntegerAdditionModulo2Pow32(4294967297L,4294967298L)));
	}
}
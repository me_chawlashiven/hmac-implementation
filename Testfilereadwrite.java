import java.io.*;
import java.lang.StringBuilder;

class Testfilereadwrite
{
	public static void main(String[] args)
	{
		//1 million '61' Test Case
		StringBuilder testText = new StringBuilder();
		for(long l = 0; l < 10000000; l++)
		{
			testText.append("61");			
		}
		//String testWrite = "Hello World. \nWrite Successful!\n" + testText.toString();
		String testWrite = testText.toString();
		//Test Case Successful!
		(new writeFileClass()).writeFile("temp.java",testWrite);
		
		/*
		if((new writeFileClass()).writeFile("temp.java",testWrite))
		{
			System.out.println((new readFileClass()).readFile("temp.java"));
		}
		*/
	}
}

class writeFileClass2
{
	boolean writeFile(String fileName, String writeData)
	{
        try {
            // Used wrap FileWriter in BufferedWriter.
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName));

            bufferedWriter.write(writeData);
            
			//Closing file.
            bufferedWriter.close();
			return true;
        }
        catch(IOException ex) {
            System.out.println("Error writing to file '" + fileName + "'");
            ex.printStackTrace();
			return false;
        }
	}
}

class readFileClass2
{
	String readFile(String fileName)
	{
        // This will reference one line at a time
        String line = null;
		StringBuilder fullText = new StringBuilder();

        try 
		{
            // Used wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));

            while((line = bufferedReader.readLine()) != null)
			{
                fullText.append(line);
            }   

            //Closing files.
            bufferedReader.close();
			return fullText.toString().replaceAll("\\s+","");
        }
        catch(FileNotFoundException ex)
		{
            System.out.println("Unable to open file '" + fileName + "'");
			return null;
        }
        catch(IOException ex)
		{
            System.out.println("Error reading file '" + fileName + "'");
            ex.printStackTrace();
			return null;
        }
    }
}

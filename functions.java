class functions
{
	int[] binaryKi = {0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174, 0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da, 0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85, 0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070, 0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2};
	
	//Addition Modulo 2^32 function
	long Bound = 0x100000000L;
	
	//number of blocks of 512 bit words of input message
	long numberOfBlocks = 0;
	
	//Elementary Functions
	
	long Ch_function(long X, long Y, long Z)
	{
		return (X&Y)^(~X&Z);
	}
	long Maj_function(long X, long Y, long Z)
	{
		return (X&Y)^(X&Z)^(Y&Z);
	}
	long IntegerAdditionModulo2Pow32(long A, long B)
	{
		return (A+B)%Bound;
	}
	long RotR(long bits, long n)
	{
		return (bits >>> n) | (bits << (Integer.SIZE-n));
	}
	long ShR(long bits, long n)
	{
		return bits>>>n;
	}
	String A_concate_B(long A, long B, int base)
	{
		StringBuilder str = new StringBuilder();
		str.append(Long.toString(A, base));
		str.append(Long.toString(B, base));
		return str.toString();
	}
	long GreaterSigma_Zero_function(long X)
	{
		return RotR(X,2)^RotR(X,13)^RotR(X,22);
	}
	long GreaterSigma_One_function(long X)
	{
		return RotR(X,6)^RotR(X,11)^RotR(X,25);
	}
	long LowerSigma_Zero_function(long X)
	{
		return RotR(X,7)^RotR(X,18)^RotR(X,3);
	}
	long LowerSigma_One_function(long X)
	{
		return RotR(X,17)^RotR(X,19)^RotR(X,10);
	}
	
	
	public static void main(String[] args)
	{
		System.out.println((new functions()).LowerSigma_One_function(61626364));
	}
}
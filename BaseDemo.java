class BaseDemo
{
	public static void main(String args[])
	{
		int octal25=031;
		int hex25=0x19;
		System.out.println("031 (octal) to decimal: " + octal25);
		System.out.println("0x19 (hexaDecimal) to decimal: " + hex25);
		System.out.println();
		
		long decimalValue = Long.parseLong(String.format("%x",0xb5c0fbcf),16);
		System.out.println("0xb5c0fbcf to decimal: " + decimalValue);
		System.out.println();
		
		System.out.println("Octal representation of ten: " + Integer.toString(10,8));
		System.out.println("hexadecimal representation of ten: " + Integer.toString(10,16));
		System.out.println("Binary representation of ten: " + Integer.toString(10,2));
		System.out.println("Decimal representation of ten: " + Integer.toString(10,10));
		System.out.println();
		
		String st="b5c0fbcf";//b5c0fbcf
		System.out.println("Decimal representation of the hexString '" + st + "': " + Long.parseLong(st,16));
		System.out.println("hexaDecimal representation of the Decimal '" + Long.parseLong(st,16) + "': " + Long.toHexString(Long.parseLong(st,16)));
		System.out.println();
		
		long bin_long = Long.parseLong(Long.toBinaryString(Long.parseLong(st,16)),2);
		System.out.println("Binary representation of the hexString '" + st + "': " + Long.toBinaryString(bin_long));
		System.out.println("hexaDecimal representation of the binaryString '" + Long.toBinaryString(bin_long) + "': " +Long.toHexString(bin_long));
		System.out.println();
		
		int binaryInt = 00000011;
		System.out.println(binaryInt);
		//System.out.println("Decimal representation of the binaryString of hexString'" + st + "': " + Long.parseLong(Long.toString(Long.parseLong(st,16),2),2));
	}
}
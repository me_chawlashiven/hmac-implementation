class Rightcircularshift
{
	int RotR(int bits, int n)
	{
		return (bits >>> n) | (bits << (Integer.SIZE-n));
	}
	public static void main(String args[])
	{
		int x = 2;
		int y = 3;
		System.out.println(x + " shifted by y bits: " + (new Rightcircularshift()).RotR(x,y));
	}
}
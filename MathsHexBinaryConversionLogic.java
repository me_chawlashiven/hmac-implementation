/*
random number 156840 hex digits
156840 * 4 = 627360 binary digits
448 Corresponds to 112

number of blocks in binary = Floor (627360 % 512) = 1225.3125
number of blocks = 1225+1 = 1226

number of blocks in hex = Floor(156840 % 128) = 1225.3125
number of blocks = 1225+1 = 1226
*/